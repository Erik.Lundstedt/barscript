#!/bin/env fennel
;; -*- tab-width: 2; indent-tabs-mode: t; electric-indent-mode: t; -*-

(var util {})

(fn util.btn [command icon]
	(local vals [" %{A:" command "&:}" icon "%{A} "] )
	(table.concat vals)
	)
(fn util.screen [index]
	(.. "%{S" index "} ")
	)
(fn util.bg [colour]
	(.. "%{B" colour "} ")
	)
(fn util.fg [colour]
	(.. "%{F" colour "} ")
	)
(fn util.left []
	""
	"%{l} "
	)
(fn util.center []
	""
	"%{c} "
	)
(fn util.right []
	""
	"%{r} "
	)
(fn util.shell [cmd]
	(local f (assert (io.popen cmd "r")))
	(local s (assert (f:read "*a")))
	(f:close )
	;;s
	(string.gsub s "\n" "")
	)

;;local s = file.readAll() -- file is the file handle
;;local result = string.gsub(s, "n", "") -- remove line breaks
;;print(result)


;;(print ((. util 2)) )
;;(print   ((. util "btn") "st -e tmuxfzf" "[>_]" ) )




;;



(fn playPause []
	(util.shell "playerctl status")
	)




(local musicStatus
		 [
			"["
			(playPause)
			((. util "shell") "playerctl metadata xesam:title" )
			"by"
			((. util "shell") "playerctl metadata xesam:artist")
			"]"
			]
		 )

(local left
		 [
			(util.screen "1")
			(util.left)
			(util.btn "jgmenu" "|[E]|")
			]
		 )
(local center
		 [
			(util.screen "1")
			(util.center)
			(util.btn "tym --role tmp " "[>_]")
			(util.btn "i3lock -c 232635" "[lock]")
			(util.btn "luakit" "[0]")
			;;(util.btn "tym --name tmp -e wallSet $(numselect 2|dmenu -c -l 10)" "[UWU]")
			(util.btn "setwall" "[UWU]")
			(util.btn "tym --role 'tmp' --width=130 --height=42 -e 'w3m wttr.in/sundsvall'" "[W]")
			])

(local right
		 [
			(util.screen "1")
			(util.right)
			(util.btn "playerctl play-pause"	(table.concat musicStatus " "))
			(util.btn "neon-logout" "[Q]")
			;;(util.btn "echo \"quit\">/tmp/dwm.fifo" "[:q]")
			]
		 )
(local config
		 [
			(util.screen 1)
			(table.concat left)
			(table.concat center)
			(table.concat right)
			]
		 )


;;st -g=130x42 webLite wttr.in/sundsvall

(fn main [] ;;"Standard main function."
	(match (. arg 1)
		:left
		(fn []
			(print (table.concat left))
			)
		:center
		(fn []
			(print (table.concat center))
			)
		:right
		(fn []
			(print (table.concat right))
			)
		)
	(print (table.concat config))
	)

(main)
